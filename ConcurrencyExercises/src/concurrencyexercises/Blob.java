/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concurrencyexercises;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author steven
 */
class Blob
{

    int x, y, radius;
    Color color;

    Blob(int x, int y, Color c)
    {
        this(x, y, 100, c);
    }

    Blob(int x, int y, int radius, Color c)
    {
        this.x = x;
        this.y = y;
        this.radius = radius;

        setColor(c);
    }

    public void draw(Graphics g)
    {
        System.out.println("in blob paint");
        erase(g);
        g.setColor(color);
        g.fillOval(x, y, 100, 100);
        g.setColor(Color.BLACK);
    }

    public void setColor(Color c)
    {
        color = c;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public void setX(int x)
    {
        this.x = x;
    }

    public void setY(int y)
    {
        this.y = y;
    }

    private void erase(Graphics g)
    {
        g.clearRect(x, y, 100, 100);
    }
}
