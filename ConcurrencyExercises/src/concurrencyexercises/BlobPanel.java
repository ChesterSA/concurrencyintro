/*
 * Created in Teesside University
 * By Chester Swann-Auger
 * As part of Group B - Airport Routing Controller
 */
package concurrencyexercises;

import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author s6089488
 */
class BlobPanel extends JPanel {

    ArrayList<Blob> blobs = new ArrayList<>();
    
    public BlobPanel()
    {
    }
    
    public void paint(Graphics g) {
        super.paint(g);
        for(Blob b : blobs) {
            b.draw(g);
        }
    }
}
