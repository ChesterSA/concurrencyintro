/*
 * Created in Teesside University
 * By Chester Swann-Auger
 * As part of Group B - Airport Routing Controller
 */
package concurrencyexercises;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JFrame;

/**
 *
 * @author s6089488
 */
public final class BlobFrame extends JFrame
{
    final int FRAME_WIDTH = 500;
    final int FRAME_HEIGHT = 200;
    BlobPanel blobPanel;
    int x = 50;
    final static int THREAD_PAUSE_INTERVAL = 40;
    
    public BlobFrame()
    {
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,150);
        setVisible(true);
        
        blobPanel= new BlobPanel();
        add(blobPanel);
        
        ArrayList<Blob> blobs = new ArrayList<>();
        
        blobs.add(new Blob(50,50, Color.RED));
        blobs.add(new Blob(50,150, Color.MAGENTA));
        
        exec(blobs);
        
    }
    
    public void exec(final ArrayList<Blob> blobs)
    {
        ArrayList<Thread> threads = new ArrayList<>();

        for (Blob b : blobs)
        {
            Thread t = new Thread(
                    new Runnable()
            {
                Blob myBlob = b;

                @Override
                public void run()
                {

                    while (blobPanel.isShowing())
                    {
                        int newX = myBlob.getX() + 10;
                        
                        Utils.pause(200);

                        myBlob.setX(newX);
                        
                        
                    }
                }
            }
            );

            threads.add(t);
        }

        threads.add(
                new Thread(
                        new Runnable()
                {
                    @Override
                    public void run()
                    {
                        while (blobPanel.isShowing())
                        {
                            System.out.println("location 2");
                            blobPanel.repaint();

                            Utils.pause(THREAD_PAUSE_INTERVAL);
                        }
                    }
                }
                )
        );

        //start all the threads
        for (Thread t : threads)
        {
            System.out.println("location 3");
            t.start();
        }

    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);

        
    }
    
}
